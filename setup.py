#!/usr/bin/env python

""" ezbaker setup.py. """

from setuptools import setup

setup(
    name='ezbaker',
    version='1.0',
    packages=['ezbaker'],
    include_package_data=True,
    install_requires=[
        'gpiozero',
    ],
)
