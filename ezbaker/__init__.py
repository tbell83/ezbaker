""" Manage lightbulb """
import glob
import logging
from time import sleep
from datetime import datetime
from gpiozero import OutputDevice

BASE_DIR = '/sys/bus/w1/devices/'
DEVICE_FOLDER = glob.glob(BASE_DIR + '28*')[0]
DEVICE_FILE = DEVICE_FOLDER + '/w1_slave'
GPIO_PIN = 14
TARGET_TEMP = 80
RELAY = OutputDevice(GPIO_PIN, active_high=True, initial_value=False)
LOGFILE = '/var/log/ezbaker.log'

logging.basicConfig(filename=LOGFILE, filemode='w+', format='%(message)s')


def read_temp_raw():
    """ Get Raw Temp """
    temp_file = open(DEVICE_FILE, 'r')
    lines = temp_file.readlines()
    temp_file.close()
    return lines


def read_temp():
    """ Get Temp Fahrenheit """
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = round(temp_c * 9.0 / 5.0 + 32.0, 2)
        return temp_f


while True:
    NOW = datetime.now()
    TEMPERATURE = read_temp()
    logging.info('%s\t%s\t%s', NOW.strftime("%Y-%m-%d %H:%M"),
                 TEMPERATURE, "On" if RELAY.value == 1 else "Off")
    if NOW.minute % 5 == 0:
        if TEMPERATURE >= TARGET_TEMP and RELAY.value == 1:
            logging.info(
                'Current temperature of %s at or above target temp of %s, turning off bulb.',
                TEMPERATURE, TARGET_TEMP
            )
            RELAY.off()
        elif TEMPERATURE < TARGET_TEMP - 5 and RELAY.value == 0:
            logging.info(
                'Current temp of %s below target range of %s - %s, turning on bulb.',
                TEMPERATURE, TARGET_TEMP-5, TARGET_TEMP
            )
            RELAY.on()
    sleep(60)
