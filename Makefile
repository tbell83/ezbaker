SHELL = /bin/bash
VIRTUALENV_DIR = ./venv

dev: virtualenv ## Activate virtualenv and editable install with dev requirements
		source ${VIRTUALENV_DIR}/bin/activate && \
				pip install -U pip && \
				pip install --editable . && \
				pip install -r requirements.txt

virtualenv:
		mkdir -p ${VIRTUALENV_DIR}
		python3 -m venv ${VIRTUALENV_DIR}