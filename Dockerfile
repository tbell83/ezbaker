FROM python:alpine

WORKDIR /home/ezbaker

COPY . .

RUN \
    pip install --upgrade pip && \
    pip install -r requirements.txt

ENTRYPOINT [ "/usr/bin/python3", "/home/ezbaker/ezbaker/__init.py__" ]
